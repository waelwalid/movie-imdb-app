# Movie APP (IMDB)

Hi ! This Repository for Movie APP like IMDB 

 1. 1 - Clone Repository "git clone
    git@bitbucket.org:waelwalid/movie-imdb-app.git"
 2. 2 - cd movie-imdb-app
 3. 3 - composer install
 4. 4 - Rename .env.example to .env
 5. 5 - php artisan key:generate
 6. 6 - Set Database : DB_DATABASE=   <YOUR DB NAME> DB_USERNAME= <YOUR
    DB USERNAME> DB_PASSWORD= <YOUR DB PASSWORD>
 7. 7 - php artisan migrate --seed
 8. 8 - php artisan passport:client --personal
 9. 9 - php artisan serve
 10. 10 - Open your Rest APP - Example POST MAN :
     https://www.getpostman.com/downloads/
 11. 11 - Get Access Token via -> this URI :
     "http://127.0.0.1:8000/api/login" Method : **POST** Body : **email
     & password** ( Both are required )
     **Note : you will use email and password from your DB (users table)**

**API Documentation :**
link : https://documenter.getpostman.com/view/1239842/RznEKeP9


> You can find more information about **Movie APP** Live Version[here](https://w-movie.herokuapp.com/).

```