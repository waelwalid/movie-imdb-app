<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::Group(['prefix' => LaravelLocalization::setLocale(null, 2) ,'namespace' => 'Api\V1'],function (){
    Route::Group(['middleware' => ['auth:api']],function(){
        Route::Resource("movie" , "MovieController");
        Route::get("/logout" , "AuthController@logout");

    });

    Route::post("/login" , "AuthController@login");



});
