<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title' , 'description' , 'image' , 'rate' , 'release' , 'director_id' , 'gross_profit'];
    public function rate(){
        return $this->hasMany("App\MovieRate");
    }
    public function director(){
        return $this->belongsTo('App\Director');
    }
    public function actors(){
        return $this->belongsToMany('App\Actor','movie_actors');
    }
    public function genres(){
        return $this->belongsToMany('App\Genre','movie_genres');
    }
}
