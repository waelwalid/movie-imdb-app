<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Mockery\Exception;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use App\Movie ;
use App\MovieActor ;
use App\MovieGenre ;
use App\Filters\FiltersMovieGenre ;
use App\Http\Requests\MovieStore ;
use App\Http\Requests\MovieUpdate ;

class MovieController extends BaseController
{

    /**
     * Display a listing of the resource.
     * With : filter / sort / include
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        try{

            $movies = QueryBuilder::for(Movie::class)
                ->allowedSorts(['title', "release" , "gross_profit" , "rate"] )
                ->allowedFilters(
                    Filter::exact('director_id'),
                    Filter::exact('release'),
                    Filter::exact('gross_profit'),
                    'title',
                    Filter::exact("rate"),
                    Filter::custom('genre', FiltersMovieGenre::class)
                )
                ->allowedIncludes('actors','director','genres' , "rate")->get();

            return $this->response($movies);

        }catch(\Exception $exception){
             return $this->response(null , 400 , $exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieStore $request)
    {
        try{
            $movie  =   new Movie();
            if($insertedMovie = $movie->create($request->only(['title','description','image','release','rate','director_id','gross_profit']))){

                /** Save Genres */
                $genres = [] ;
                foreach($request->genre as $value){
                    $genres[] = ["genre_id" => $value , "movie_id" => $insertedMovie->id];
                }
                //dd($genres);
                $genres = MovieGenre::insert($genres);

                /** Save Actors */
                $actors = [] ;
                foreach($request->actor as $value){
                    $actors[] = ["actor_id" => $value , "movie_id" => $insertedMovie->id];
                }
                $actors = MovieActor::insert($actors);
                return $this->response($movie->toArray(),200,"Created");
            }
        }catch(Exception $exception){
            return $this->response(null,400,$exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $movie = Movie::find($id);
            if(!$movie){
                return $this->response([],404,"Not Found");
            }else{
                return $this->response($movie->toArray(),200,"Single");
            }

        }catch(Exception $exception){
            return $this->response(null,400,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MovieUpdate $request, $id)
    {
        if(!$id){
            return $this->response(null,400,"url / {id} variable si required");
        }
        try{
            $movie  =   Movie::find($id);
            if($movie->update($request->only(['title','description','image','release','rate','director_id','gross_profit']))){

                /** @var Clear Old $genres */
                $genres = MovieGenre::where("movie_id" , $id)->delete($request->genre);
                /** Save Genres */
                $genres = [] ;
                foreach($request->genre as $value){
                    $genres[] = ["genre_id" => $value , "movie_id" => $id];
                }

                $genres = MovieGenre::insert($genres);

                /** @var Clear Old $genres */
                $actors = MovieActor::where("movie_id" , $id)->delete($request->genre);
                /** Save Actors */
                $actors = [] ;
                foreach($request->actor as $value){
                    $actors[] = ["actor_id" => $value , "movie_id" => $id];
                }
                $actors = MovieActor::insert($actors);

                return $this->response($movie->toArray(),200,"Updated");
            }
        }catch(Exception $exception){
            return $this->response(null,400,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $movie = Movie::find($id);
            if(!$movie){
                return $this->response([],404,"Not Found");
            }
            if($movie->delete()){
                $actors = MovieActor::where("movie_id")->delete();
                $genre  = MovieGenre::where("movie_id")->delete();
                return $this->response([],200,"Deleted");
            }
        }catch(Exception $exception){
            return $this->response(null,400,$exception->getMessage());
        }
    }

}
