<?php
namespace App\Http\Controllers\Api\V1;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Requests\UserLogin;
use Carbon\Carbon;

class AuthController extends BaseController
{

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(UserLogin $request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $accessToken =  $user->createToken('MyApp');
            $expires_at  = $accessToken->expires_at = Carbon::now()->addDays(1);

            return $this->response([
                'token' => $accessToken->accessToken ,
                'token_type' => 'Bearer',
                'expires_at' => $expires_at
            ],200);
        }
        else{

            return $this->response(['error'=>'Unauthorised'],401);
        }
    }

    public function logout(Request $request){
        $request->user()->token()->revoke() ;
        return response()->json(['msg' => 'token has been revoked'] , 200) ;
    }

}