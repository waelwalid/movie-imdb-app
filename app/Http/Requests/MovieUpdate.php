<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MovieUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:255",
            "description" => "required:max:1000",
            "image" => "required|max:255|url",
            "release" => "required|numeric",
            "rate" => "required|numeric",
            "director_id" => "required|exists:directors,id",
            "gross_profit" => "required|numeric",
            "genre" => "required|array",
            "actor" => "required|array",
            "genre.*" => "required|exists:genres,id|numeric",
            "actor.*" => "required|exists:actors,id|numeric",
        ];
    }
}
