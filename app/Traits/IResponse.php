<?php
/**
 * Created by PhpStorm.
 * User: Wael
 * Date: 1/5/2019
 * Time: 10:36 PM
 */

namespace App\Traits;


trait IResponse
{
    public function response($data = null , $code = null , $msg = ''){
        if((is_array($data) OR is_object($data)) AND count($data)){
            if(!$code){
                $code = 200 ;
            }
        }elseif((is_array($data) OR is_object($data)) AND !count($data)){
            if(!$code){
                $code = 404 ;
            }
        }

        return response()->json(['code' => $code , 'data' => $data , 'msg' => $msg] , $code);
    }
}