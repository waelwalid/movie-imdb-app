<?php
/**
 * Created by PhpStorm.
 * User: TheSpecialOne-
 * Date: 1/6/2019
 * Time: 12:23 PM
 */

namespace App\Localization;


use Mcamara\LaravelLocalization\LaravelLocalization as McamaraLocalization;

class LaravelLocalization extends McamaraLocalization
{
    public function setLocale($locale = null, int $segment = 1)
    {
        if (empty($locale) || !is_string($locale)) {
            $locale = $this->request->segment($segment);
        }
        return parent::setLocale($locale);
    }
}