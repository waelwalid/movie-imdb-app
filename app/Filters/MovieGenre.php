<?php
/**
 * Created by PhpStorm.
 * User: Wael
 * Date: 1/5/2019
 * Time: 8:23 PM
 */

namespace App\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersMovieGenre implements Filter
{
    public function __invoke(Builder $query, $value, string $property) : Builder
    {
        return $query->whereHas('genres', function (Builder $query) use ($value) {
            $query->where('title', $value);
        });
    }
}

