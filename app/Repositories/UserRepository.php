<?php
/**
 * Created by PhpStorm.
 * User: Wael
 * Date: 1/4/2019
 * Time: 5:31 PM
 */

namespace App\Repositories;


class UserRepository extends Repository
{
    public function model()
    {
        return 'App\User';
    }
}