<?php
/**
 * Created by PhpStorm.
 * User: Wael
 * Date: 1/4/2019
 * Time: 5:24 PM
 */

namespace App\Repositories;


interface RepositoryInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}