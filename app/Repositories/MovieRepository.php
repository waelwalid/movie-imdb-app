<?php
/**
 * Created by PhpStorm.
 * User: Wael
 * Date: 1/5/2019
 * Time: 4:16 AM
 */

namespace App\Repositories;


class MovieRepository extends Repository
{
    public function model()
    {
        return 'App\Movie';
    }
}