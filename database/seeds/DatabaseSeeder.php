<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 5)->create();
        factory(App\Actor::class, 100)->create();
        factory(App\Director::class, 100)->create();
        factory(App\Movie::class, 30)->create();
        factory(App\Genre::class, 7)->create();
        factory(App\MovieActor::class, 100)->create();
        factory(App\MovieGenre::class, 100)->create();

    }
}
