<?php

use Faker\Generator as Faker;

$factory->define(App\MovieActor::class, function (Faker $faker) {
    return [
        'movie_id' => function(){
            return App\Movie::inRandomOrder()->first()->id;
        },
        'actor_id' => function(){
            return App\Actor::inRandomOrder()->first()->id;
        }
    ];
});
