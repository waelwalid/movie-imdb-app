<?php

use Faker\Generator as Faker;

$factory->define(App\MovieGenre::class, function (Faker $faker) {
    return [
        'movie_id' => function(){
            return App\Movie::inRandomOrder()->first()->id;
        },
        'genre_id' =>function(){
            return App\Genre::inRandomOrder()->first()->id;
        }
    ];
});
