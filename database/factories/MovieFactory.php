<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->randomElement(
            [
                "Star Wars: The Last Jedi",
                "Beauty and the Beast",
                "The Fate of the Furious",
                "Despicable Me 3",
                "Jumanji: Welcome to the Jungle",
                "Spider-Man: Homecoming",
                "Wolf Warrior 2",
                "Guardians of the Galaxy Vol. 2",
                "Thor: Ragnarok" ,
                "Wonder Woman",
                "The Godfather",
                "The Shawshank Redemption",
                "Schindler's List",
                "Raging Bull",
                "Casablanca",
                "Citizen Kane",
                "Gone with the Wind",
                "Captain America: Civil War",
                "Rogue One: A Star Wars Story",
                "Finding Dory",
                "Zootopia",
                "The Jungle Book",
                "The Secret Life of Pets",
                "Batman v Superman: Dawn of Justice",
                "Fantastic Beasts and Where to Find Them",
                "Deadpool",
                "Suicide Squad",
                "Coffee with D",
                "Angry Men",
                "Angry Woman"

            ]
        ),
        'description' => $faker->text(200),
        'image' => $faker->imageUrl(640,480),
        'release' => $faker->year,
        'gross_profit' => rand(10000000,300000000),
        'director_id' =>function(){
            return App\Director::inRandomOrder()->first()->id;
        },
        'rate' => rand(5 , 10)
    ];
});
